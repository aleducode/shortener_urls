"""Hacku Api."""

# Django
from django.conf import settings

# Utilities
import urllib
import requests
import json


class HackuWrapper:
    """Wrapper for hacku service."""

    def _get_resource(self, uri, **kwargs):
        """Get resource method."""
        username = kwargs.pop('username')
        url = "{}/api/{}/{}".format(
            settings.HACKU_URL,
            uri,
            username
        )
        # Headers
        headers = {
            "Authorization": "Api-Key {}".format(settings.HACKU_API_KEY_CLIENT),
            "Content-Type": "application/json"
        }
        r = requests.get(url, headers=headers, timeout=10)
        return r

    def _post_resource(self, uri, **kwargs):
        """Post resource method."""
        url = "{}/api/{}/".format(settings.HACKU_URL, uri)
        # Headers
        headers = {
            "Authorization": "Api-Key {}".format(settings.HACKU_API_KEY_CLIENT),
            "Content-Type": "application/json"
        }
        r = requests.post(
            url,
            data=json.dumps(kwargs),
            timeout=100,
            headers=headers)
        return r

    def _get_json_resource(self, uri, **kwargs):
        try:
            response_json = {}
            method = kwargs.pop('method')
            if method == 'get':
                response = self._get_resource(uri, **kwargs)
            else:
                response = self._post_resource(uri, **kwargs)
            response.raise_for_status()
            if response.status_code in [requests.codes.ok, requests.codes.created]:
                response_json = response.json()
        except requests.exceptions.RequestException as e:
            status_code = getattr(e.response, "status_code", None)
            reason = getattr(e.response, "reason", None)
            response_json = {
                "status_code": status_code,
                "message": reason,
                }
        return response_json

    def get_user_shortener(self, username):
        """Get user shortened."""
        uri = 'users'
        method = 'get'
        return self._get_json_resource(
            uri,
            username=username,
            method=method
        )

