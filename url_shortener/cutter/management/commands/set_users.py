"""Check jail Command."""

# Django
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.utils import timezone
from django.conf import settings

# Models
from url_shortener.cutter.models import (
    Urls
)
# Api
from url_shortener.utils.hacku_api import HackuWrapper

# Utils
import urllib.parse as urllib_parse
import jwt
from datetime import datetime


class Command(BaseCommand):
    """Check history notification base command."""

    def handle(self, *args, **options):
        """Handle command usage."""
        hacku = HackuWrapper()
        for url in Urls.objects.all():
            long_url = url.http_url
            if 'token' in long_url  and any(env in long_url for env in settings.PRODUCTION_ENVIRONMENT):
                parsed_url = urllib_parse.urlparse(long_url).query
                token = urllib_parse.parse_qs(parsed_url).get('token')[0]
                payload = jwt.decode(token, settings.KEY_JWT_ENCRYPT, algorithms=['HS256'])
                response = hacku.get_user_shortener(payload.get('user'))
                url.user_id = response.get('pk')
                url.expired_at = datetime.fromtimestamp(payload.get('exp'))
                url.username = payload.get('user')
                url.save(update_fields=['user_id', 'expired_at', 'username'])
        self.stdout.write(self.style.SUCCESS('OK'))
