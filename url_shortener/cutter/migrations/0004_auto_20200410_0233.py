# Generated by Django 3.0.5 on 2020-04-10 07:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cutter', '0003_auto_20200408_2324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urls',
            name='http_url',
            field=models.URLField(max_length=2000),
        ),
    ]
