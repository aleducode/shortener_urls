"""Cutter Models."""

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django_extensions.db.models import TimeStampedModel
from django.db import models


class User(TimeStampedModel, AbstractUser):
    """User model.

    Extend from Django's Abstract User, change the username field
    to email and add some extra fields.
    """

    email = models.EmailField(
        'email address',
        unique=True,
        error_messages={
            'unique': 'Ya existe un usuario con ese email.'
        }
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        """Return username."""
        return self.username

    def get_short_name(self):
        """Return username."""
        return self.username

    class Meta:
        """Meta class."""

        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"


class Urls(TimeStampedModel, models.Model):
    """Urls Models."""

    http_url = models.URLField(max_length=2000)
    counter = models.IntegerField(default=0)
    short_uri = models.SlugField()
    user_id = models.IntegerField('User', null=True, blank=True)
    username = models.CharField('Username', max_length=255, null=True, blank=True)
    expired_at = models.DateField(
        'expiration',
        null=True,
        blank=True,
        auto_now=False,
        auto_now_add=False
    )

    @property
    def shorted_url(self):
        return '{}/{}'.format(
                settings.SITE_URL,
                self.short_uri
            )

    class Meta:
        verbose_name = 'Url shortened'
        verbose_name_plural = 'Urls shortened'

    def __str__(self):
        """Return username."""
        return self.short_uri
