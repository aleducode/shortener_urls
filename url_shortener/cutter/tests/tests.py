
"""Cutter test."""

# Django
from django.test import TestCase
from rest_framework_api_key.models import APIKey

# Model
from url_shortener.cutter.models import Urls

# Django Rest Framework
from rest_framework import status
from rest_framework.test import APITestCase


class ShortenerUrlAPITestCase(APITestCase):
    """ShortenerUrlAPITestCase API test case."""

    def setUp(self):
        """Test case setup."""
        api_key, key = APIKey.objects.create_key(name="test-service")

        # Auth
        self.apikey = key
        self.client.credentials(HTTP_AUTHORIZATION='Api-Key {}'.format(self.apikey))

        # URL
        self.url = '/api/shortener/'


    def test_shortener_url_success(self):
        """Verify request succees."""
        url_to_short = 'https://foo.com'
        request = self.client.post(self.url, data={'url_to_short': url_to_short})
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)

