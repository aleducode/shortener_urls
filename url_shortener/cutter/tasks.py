from django.contrib.auth import get_user_model
from django.conf import settings
from config import celery_app
from url_shortener.cutter.models import Urls
from celery.decorators import task

# Utils
import urllib.parse as urllib_parse
import jwt
from datetime import datetime


@task(name='set_shortened_user')
def set_shortened_user(shortened_obj_pk):
    """Idenfity user on shortener link."""
    try:
        shortened = Urls.objects.get(pk=shortened_obj_pk)
    except Urls.DoesNotExist:
        shortened = None
    if shortened:
        long_url = shortened.http_url
        # Retrieve user data only from production urls
        if 'token' in long_url  and any(env in long_url for env in settings.PRODUCTION_ENVIRONMENT):
            parsed_url = urllib_parse.urlparse(long_url).query
            token = urllib_parse.parse_qs(parsed_url).get('token')[0]
            payload = jwt.decode(token, settings.KEY_JWT_ENCRYPT, algorithms=['HS256'])
            date_exp = datetime.fromtimestamp(payload.get('exp'))
            user_pk = payload.get('user_pk')
            shortened.user_id = user_pk
            shortened.expired_at = date_exp
            shortened.username = payload.get('user')
            shortened.save(update_fields=['user_id', 'expired_at', 'username'])
            return f'{shortened.short_uri} updated successfully.'

